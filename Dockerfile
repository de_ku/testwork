FROM python:3.8

RUN useradd -ms /bin/bash dockeruser
USER dockeruser
WORKDIR /app

RUN pip install --upgrade pip
COPY . /app
RUN pip install --no-cache -r requirements/production.txt

#COPY dist /tmp/dist
#RUN pip install /tmp/dist/*

EXPOSE 8085

CMD ["python3", "run_service.py", "--port", "8085"]



